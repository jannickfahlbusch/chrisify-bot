// https://www.opsdash.com/blog/slack-bot-in-golang.html

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

var (
	token    string
	chrisify string
	haar     string
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "usage: slackbot slack-bot-token\n")
		os.Exit(1)
	}

	token = os.Args[1]
	chrisify = "/go/bin/chrisify"
	haar = "/chrisify/haarcascade_frontalface_alt.xml"

	// start a websocket-based Real Time API session
	ws, id := slackConnect(token)
	fmt.Println("slackbot ready, ^C exits")

	for {
		// read each incoming message
		m, err := getMessage(ws)
		if err != nil {
			log.Fatal(err)
		}

		// see if we're mentioned
		log.Printf("From %+v , i am %v\n", m, id)
		if m.Type == "message" && m.SubType == "file_share" && m.User != id {
			go func(m Message) {
				var channel string
				json.Unmarshal(m.Channel, &channel)
				file := SaveTempFile(GetFile(m.File))
				chrisd := Chrisify(file)
				log.Printf("Uploading to %s", channel)
				Upload(chrisd, channel)

				defer os.Remove(file)
			}(m)
		}
	}
}
